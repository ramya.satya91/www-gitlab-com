---
layout: handbook-page-toc
title: Home Page for Support's Ginkgo Group
description: Home Page for Support's Ginkgo Group
---

<!-- Search for all occurrences of NAME and replace them with the group's name.
     Search for all occurrences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Ginkgo group

Introductory text, logos, or whatever the group wants here

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ginkgo resources

- Our Slack Channel: [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G)
  - As a member of the Ginkgo group, consider [changing your notifications settings](#slack-notifications) to allow for better collaboration.
- Our Team: [Ginkgo Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=ginkgo)

## Ginkgo workflows and processes

#### Daily Crush Sessions

EMEA/AMER friendly crush sessions are held every weekday from 14:30 to 15:30 UTC.

**Goal**:
 
Meet as a group and work toward our [Definition of Success](../index.html.md#success-of-their-group-means).

**Guidelines**:

Crush Sessions are usually a good opportunity to "crush" the FRT/NRT queues by working on tickets using a top down approach. E.g., the group picks up the next ticket closest to a breach and works together to drive it toward a resolution.
Crush Sessions are **not** meant as a substitute for monitoring our FRT/NRT queues throughout the day. Tickets can and certainly will breach between crush sessions if we don't monitor our queues during our respective work hours. 

Unassigned tickets will be assigned based on a round robin of the attendants, their support region, and current capacity. The standard [Working on Tickets](../../../workflows/working-on-tickets.html) workflow still applies. 

_**These are just guidelines**_. If you feel overloaded, you should opt out of the round robin and remain in the crush session to assist. Let your colleagues know as you join the Crush Session, e.g., "I can't take on tickets today, but I'd love to help with internal comments and the like."

**Location**:

These meetings will be held in the Gingko Collab Room, which can be found in the bookmarks of Our Slack Channel, [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G).

#### Slack notifications

To update your notification settings on Slack:
1. In Slack, right click on our channel [#spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G)
2. Choose **Change notifications**
3. Change **Send a notification for** to **All new messages**
4. Save Changes

